
# Maxigo - Five-in-a-row/Gomoku/Wuziqi game for the Arduboy

__Play it online in the Arduboy emulator:__
[click here](https://felipemanga.github.io/ProjectABE/?url=https://gitlab.com/nowozin/maxigoarduboy/-/raw/main/release/v1.2/maxigo.v1.2.hex)

## Game description

The game is a classic turn-based zero-sum game with two players each taking
turns in placing a single stone of differing color; the first player to have
five or more stones in a row (diagonal/horizontal/vertical) wins. The
traditional board size is a 15-by-15 board, but because of limited screen space
I had to use a 13-by-13 board to keep things readable.

## Game modes

* _Two player mode_: two human players take turns in placing a stone; the first
  player to have five or more stones in a row (diagonal/horizontal/vertical)
  wins.
* _One player mode_: the second player is played by an AI (there is an "easy"
  AI and a “hard” AI).


# Technical details

* The AI is an alpha-beta search NegaMax method based on my prior
  implementation of the Javascript version of Maxigo. The search procedure is
  standard but there is some clever heuristic evaluation based on detailed
  "threat types" 1 which are recognized. The Arduboy is quite limited
  computationally and the pattern matching is pre-computed into a lookup table.
* AI playing strength: the AI is strong enough to comfortably beat beginners
  and provide a challenge to intermediate players but anyone with more
  experience in Gomoku/Wuziqi can build up longer threat sequences that AI will
  be blind to.
* Codesize versus speed: the game comfortably fits in the Arduboy space when
  compiled to save space (-Os gcc option in `platform.txt`). However, my unit
  tests showed a 2.5x speedup when I compile with optimization (-O1), but the
  full game runs out of space then. The easiest workaround was to use
  `-O1 -finline-limit=140`, where I found the value 140 by binary search (a
  large value produced faster/larger executables, a smaller value produced
  smaller/slower executables). The optimal tradeoff curve was quite smooth and
  picking `140` made the image fit into the Arduboy limitations while providing
  almost all the speedup the `-O1` unit tests demonstrated.
* History: in 2020 I built a Javascript version of the game for the 5th
  birthday of my son ("five in a row" is a matching theme), so
  it was fun to revisit the game for the Arduboy. The boy shown on the start
  screen is him, and he is actually quite good at the game.


# Contact and Bugs

If you find any bugs please file an issue in the GitLab repository, post to the
[Maxigo Arduboy community
thread](https://community.arduboy.com/t/maxigo-five-in-a-row-gomoku-wuziqi-board-game-with-good-ai/10516),
or reach out via email (`nowozin@gmail.com`).

