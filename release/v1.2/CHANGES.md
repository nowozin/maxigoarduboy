
# Changes in version 1.2, released 4th July 2022

* _AI code optimizations_.  The AI is now 40 percent faster on average.  This
  speed-up was achieved by manual optimization of the heuristic move
  evaluation.

# Changes in version 1.1, released 23rd June 2022

* _Multi-step undo/redo function_. During a game just hold down the B button
  and while holding it down move left or right to move to a prior state in the
  game. Once you have gone to a prior state but have not made any new moves yet,
  you hold `B` and move right to "redo" all the moves one-by-one. This is
  useful to slowly revisit a game after play.
* _Two AI hardness levels_: "easy" and "hard". The easy AI is similar to the
  hard AI but stochastically makes mistakes. Exploit the mistakes to win the
  game!
* _Enlarged board size_. The board is now 13x13 instead of 12x12. While
  Gomoku/Wuziqi is traditionally played on a 15x15 board, variants on 13x13 and
  19x19 are also common, so 13x13 is a good choice.
* _Different board rendering_. The lines around the board have been removed and
  instead the four corners are marked.
* Move counter. Within a game the top left displays the current move count.
  This works well with the undo/redo function but also allows to challenge
  yourself or another player in terms of how many moves are needed to beat the
  other player.
* URL changed. I changed `https://` to `http://` to make the URL work in the
  title screen.
* Version number displayed. The game now displays the version, 1.1 in the
  bottom left corner.
* More robust button handling. Moves are now made using only the `A` button, as
  the `B` button now is used for the UNDO function. Moves with the `A` button are
  now made upon release of the button, not on press.

