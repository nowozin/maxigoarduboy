
1. Make the UNDO icons adaptive:
   * show only `<-  ` when only undo is possible;
   * show only `  ->` when only redo is possible;
   * show `<-->` when redo and undo are possible.

