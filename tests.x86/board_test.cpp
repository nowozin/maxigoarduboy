
#include <cstdint>
#include <stdio.h>

#include <gtest/gtest.h>

#define	BOARD_SIZE	15
#include "../maxigoArduboy/board.h"

piece_t signMapThreats[BOARD_SIZE][BOARD_SIZE] = {
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, -1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, -1, 0, 1, 1, 1, 0, 0, 0},
	{0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1}
};

TEST(BoardTest, Orders) {
	ASSERT_EQ(5, order(TWin));
	ASSERT_EQ(4, order(TS4));
	ASSERT_EQ(4, order(T4));
	ASSERT_EQ(3, order(TS3));
	ASSERT_EQ(3, order(T3));
	ASSERT_EQ(3, order(TB3));
	ASSERT_EQ(0, order(TN));
}

// Test a few chosen lookup values to verify the lookup table is working
// properly.  The reference is the Julia implementation used to create
// the lookup table, `Gomoku Lookup Table Synthesis.ipynb`.
TEST(BoardTest, ThreatsLookup) {
	ASSERT_EQ(TN, threats_lookup(8321, STONE_BLACK));
	ASSERT_EQ(TN, threats_lookup(9345, STONE_BLACK));
	ASSERT_EQ(TN, threats_lookup(9763, STONE_BLACK));
	ASSERT_EQ(TN, threats_lookup(13387, STONE_BLACK));
	ASSERT_EQ(TN, threats_lookup(14140, STONE_BLACK));
	ASSERT_EQ(TN, threats_lookup(14584, STONE_BLACK));
	ASSERT_EQ(TN, threats_lookup(14832, STONE_BLACK));
	ASSERT_EQ(TN, threats_lookup(17755, STONE_BLACK));
	ASSERT_EQ(TN, threats_lookup(17776, STONE_BLACK));

	ASSERT_EQ(TS4, threats_lookup(1213, STONE_BLACK));
	ASSERT_EQ(TS3, threats_lookup(3630, STONE_BLACK));
	ASSERT_EQ(TWin, threats_lookup(4370, STONE_BLACK));
	ASSERT_EQ(T4, threats_lookup(9692, STONE_BLACK));
	ASSERT_EQ(T4, threats_lookup(12859, STONE_BLACK));
	ASSERT_EQ(T4, threats_lookup(14821, STONE_BLACK));
	ASSERT_EQ(TB3, threats_lookup(16495, STONE_BLACK));
	ASSERT_EQ(T3, threats_lookup(16518, STONE_BLACK));
	ASSERT_EQ(TWin, threats_lookup(19621, STONE_BLACK));

#if 0
	// White
	ASSERT_EQ(TN, threats_lookup(2659, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(3607, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(5797, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(10489, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(11017, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(13300, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(14345, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(15686, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(17939, STONE_WHITE));
	ASSERT_EQ(TN, threats_lookup(19404, STONE_WHITE));

	ASSERT_EQ(T3, threats_lookup(2933, STONE_WHITE));
	ASSERT_EQ(TWin, threats_lookup(8753, STONE_WHITE));
	ASSERT_EQ(TS4, threats_lookup(8760, STONE_WHITE));
	ASSERT_EQ(TB3, threats_lookup(14104, STONE_WHITE));
	ASSERT_EQ(T4, threats_lookup(15557, STONE_WHITE));
#endif
}

TEST(BoardTest, LookupValues) {
	Board ai(signMapThreats, STONE_BLACK);

	piece_t threat_player = STONE_BLACK;
	piece_t outside = STONE_WHITE;

	ASSERT_EQ(((lookup_t) 242),
		ai.compute_lookup_value(14, 0, 0, 1, outside, threat_player == STONE_WHITE));
	ASSERT_EQ(((lookup_t) 727),
		ai.compute_lookup_value(14, 1, 0, 1, outside, threat_player == STONE_WHITE));
	ASSERT_EQ(((lookup_t) 2182),
		ai.compute_lookup_value(14, 2, 0, 1, outside, threat_player == STONE_WHITE));
	ASSERT_EQ(((lookup_t) 6547),
		ai.compute_lookup_value(14, 3, 0, 1, outside, threat_player == STONE_WHITE));
	ASSERT_EQ(((lookup_t) 19642),
		ai.compute_lookup_value(14, 4, 0, 1, outside, threat_player == STONE_WHITE));
}

TEST(BoardTest, Threats) {
	Board ai(signMapThreats, STONE_BLACK);

	piece_t threat_player = STONE_BLACK;
	piece_t outside = STONE_WHITE;

	// Win
	ASSERT_EQ(TWin, ai.lookup_threat(14, 0, 0, 1, outside, threat_player));
	ASSERT_EQ(TWin, ai.lookup_threat(14, 1, 0, 1, outside, threat_player));
	ASSERT_EQ(TWin, ai.lookup_threat(14, 2, 0, 1, outside, threat_player));
	ASSERT_EQ(TWin, ai.lookup_threat(14, 3, 0, 1, outside, threat_player));
	ASSERT_EQ(TWin, ai.lookup_threat(14, 4, 0, 1, outside, threat_player));

	// Straight four
	ASSERT_EQ(TS4, ai.lookup_threat(8, 1, 1, 0, outside, threat_player));
	ASSERT_EQ(TS4, ai.lookup_threat(9, 1, 1, 0, outside, threat_player));
	ASSERT_EQ(TS4, ai.lookup_threat(10, 1, 1, 0, outside, threat_player));
	ASSERT_EQ(TS4, ai.lookup_threat(11, 1, 1, 0, outside, threat_player));

	// Four: oxxxx.
	ASSERT_EQ(T4, ai.lookup_threat(1, 2, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(1, 3, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(1, 4, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(1, 5, 0, 1, outside, threat_player));

	// Four: x.xxx
	ASSERT_EQ(T4, ai.lookup_threat(14, 10, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(14, 12, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(14, 13, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(14, 14, 0, 1, outside, threat_player));

	// Four: xx.xx
	ASSERT_EQ(T4, ai.lookup_threat(12, 10, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(12, 11, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(12, 13, 0, 1, outside, threat_player));
	ASSERT_EQ(T4, ai.lookup_threat(12, 14, 0, 1, outside, threat_player));

	// Straight three
	ASSERT_EQ(TS3, ai.lookup_threat(4, 9, 0, 1, outside, threat_player));
	ASSERT_EQ(TS3, ai.lookup_threat(4, 10, 0, 1, outside, threat_player));
	ASSERT_EQ(TS3, ai.lookup_threat(4, 11, 0, 1, outside, threat_player));

	// Broken three
	ASSERT_EQ(TB3, ai.lookup_threat(8, 4, 1, 1, outside, threat_player));
	ASSERT_EQ(TB3, ai.lookup_threat(10, 6, 1, 1, outside, threat_player));
	ASSERT_EQ(TB3, ai.lookup_threat(11, 7, 1, 1, outside, threat_player));

	// Three
	ASSERT_EQ(T3, ai.lookup_threat(7, 9, 0, 1, outside, threat_player));
	ASSERT_EQ(T3, ai.lookup_threat(7, 10, 0, 1, outside, threat_player));
	ASSERT_EQ(T3, ai.lookup_threat(7, 11, 0, 1, outside, threat_player));
}

TEST(BoardTest, PossibleThreats) {
	Board ai(signMapThreats, STONE_BLACK);
	piece_t threat_player = STONE_BLACK;
	piece_t outside = STONE_WHITE;

	  // Three to Four
	ASSERT_EQ(T4, ai.lookup_possible_threat(7, 8, outside, threat_player));

	// Three to Straight Four
	ASSERT_EQ(TS4, ai.lookup_possible_threat(7, 12, outside, threat_player));

	// Broken Three to Straight Four
	ASSERT_EQ(TS4, ai.lookup_possible_threat(9, 5, outside, threat_player));

	// Straight Four to Win
	ASSERT_EQ(TWin, ai.lookup_possible_threat(7, 1, outside, threat_player));
}

TEST(BoardTest, ThreatScoring) {
	ASSERT_GT(threat_to_score(TWin), threat_to_score(T4));
	ASSERT_GT(threat_to_score(T4), threat_to_score(T3));
	ASSERT_GT(threat_to_score(TB3), threat_to_score(TN));
}


piece_t signMapHeuristic[BOARD_SIZE][BOARD_SIZE] = {
	{ 0, 0, -1, 0, 0, 0, 1, 0, 0, 0, 0, 0, -1, 1, 0},
	{ 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0, 0},
	{ 0, 1, 1, 0, 0, 0, -1, 0, 0, 0, -1, 0, -1, 0, 1},
	{ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0},
	{ 1, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
	{ 1, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0},
	{ 0, 1, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{ -1, 0, 1, 0, 0, 0, -1, 1, 0, -1, 0, -1, -1, 0, 1},
	{ 1, 1, 0, 0, -1, -1, 0, -1, -1, 0, -1, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0},
	{ 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 1, -1, 0, 0, -1},
	{ 1, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1}
};

TEST(HeuristicTest, HeuristicScoring) {
	Board ai(signMapHeuristic, STONE_BLACK);

	ASSERT_EQ(-1018500, ai.heuristic_evaluation());
}

TEST(HeuristicTest, HighestThreat) {
	Board ai(signMapHeuristic, STONE_BLACK);

	ASSERT_EQ(TS3, ai.highest_existing_threat(STONE_BLACK));
	ASSERT_EQ(T4, ai.highest_existing_threat(STONE_WHITE));
}

TEST(HeuristicTest, NearbyLogic) {
	Board ai(signMapHeuristic, STONE_BLACK);
	ASSERT_EQ(false, ai.has_nearby(12, 1, 1));
	ASSERT_EQ(true, ai.has_nearby(12, 1, 2));
}

TEST(AITest, NegaMax) {
	Board ai(signMapHeuristic, STONE_BLACK);

	ASSERT_EQ(Move(-6500, 11-1, 7-1), ai.negamax(1, STONE_BLACK));
	ASSERT_EQ(Move(-2006200, 11-1, 7-1), ai.negamax(2, STONE_BLACK));
	ASSERT_EQ(Move(-1006000, 11-1, 7-1), ai.negamax(3, STONE_BLACK));
	ASSERT_EQ(Move(-100000000, 11-1, 7-1), ai.negamax(4, STONE_BLACK));

	ASSERT_EQ(Move(100000000, 11-1, 7-1), ai.negamax(1, STONE_WHITE));
	ASSERT_EQ(Move(100000000, 11-1, 7-1), ai.negamax(2, STONE_WHITE));
	ASSERT_EQ(Move(100000000, 2-1, 8-1), ai.negamax(3, STONE_WHITE));
	ASSERT_EQ(Move(100000000, 2-1, 8-1), ai.negamax(4, STONE_WHITE));
}

piece_t signMapEmpty[BOARD_SIZE][BOARD_SIZE] = {
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

TEST(AITest, NegaMaxSelfplay) {
	piece_t player = STONE_BLACK;
	Board ai(signMapEmpty, player);

	while (true) {
		move_t move = ai.negamax(2, player);
		ai.move(move.y, move.x, player);
		printf("(%d, %d)  %c\n", move.x, move.y, player == STONE_BLACK ? 'X' : 'O');
		if (ai.winner_at(move.y, move.x))
			return;

		player *= -1;
	}
}

int main(int argc, char** argv) {
	testing::InitGoogleTest(&argc, argv);
	testing::internal::CaptureStdout();

	int res = RUN_ALL_TESTS();

	std::cout << "Stdout:";
	std::string output = testing::internal::GetCapturedStdout();
	std::cout << output;

	return res;
}

