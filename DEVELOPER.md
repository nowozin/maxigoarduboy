
# Running Unit Tests

There are two sets of unit tests available for developing the Gomoku engine.
Both sets of tests are kept similar in order to allow comparison of the
x86 reference results against the Arduboy results.

## Running the x86 unit tests

This requires the Google test libraries installed (`apt-get install
googletest`).  Then run:

`cd tests.x86 && make check`

## Running the Arduboy unit tests

Steps:

1. Install the Arduino IDE (`apt-get install arduino`).
2. Open the `maxigoArduboy` sketch in the Arduino IDE.
3. Install the `AUnit` test framework using `Tools / Manage Libraries`.
4. At the top of the file, enable the `#define PERFORM_UNIT_TESTS` line.
5. In the lines below, enable one of the `TEST_XYZ` lines to select which tests
   are performed.  Typically only one or two test groups fit into the program
   memory.  For example, only enable the `#define TEST_AI` line.
6. Make sure your Arduboy is connected via USB, then open the
   _Serial Monitor_ using `Tools / Serial Monitor`.
7. Upload the sketch to your Arduboy and check the test results in the serial
   monitor window.


# Creating a Release Build

The default compilation options of the Arduino IDE are set to optimize for
small code size.  However, our AI significantly benefits from more aggressive
optimization for speed.

To build an optimized binary that still fits within the memory limitations of
the Arduboy, you have to adjust settings in the `platform.txt` file.  On my
system (Ubuntu 22.04 with the `arduino` package installed) this file is found
in the `/usr/share/arduino/hardware/arduino/avr` directory.

The relevant section to edit is the compiler flags section.  I add the
following:

```
#optimize_level=-Os
# -O1 produces good results (small but fast, much smaller than O2/O3)
optimize_level=-O1 -finline-limit=140
```

And then replace the `-Os` flag with `{optimize_level}` in three lines of the
`platform.txt` file:

```
compiler.c.flags=-c -g {optimize_level} {compiler.warning_flags} -std=gnu11 -ffunction-sections -fdata-sections -MMD -flto -fno-fat-lto-objects
compiler.c.elf.flags={compiler.warning_flags} {optimize_level} -g -flto -fuse-linker-plugin -Wl,--gc-sections
compiler.cpp.flags=-c -g {optimize_level} {compiler.warning_flags} -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -Wno-error=narrowing -MMD -flto
```

After these changes re-building in the Arduino IDE will use the optimized
settings.  If you add code you may have to adjust the limit value of `140`
downwards to preserve space.  Smaller values will prevent inlining and thus
produce less efficient code of smaller size; larger values will allow for
aggressive inlining which will benefit speed but lead to larger binaries.  I
determined the value of `140` via binary search but for version 1.2 had to
decrease it down to `40`.


