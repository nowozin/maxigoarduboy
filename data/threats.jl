@enum Threat TInvalid=-1 TN=0 TB3=1 T3=2 TS3=3 T4=4 TS4=5 TWin=6

function order(t::Threat)
    if t == TWin
        return 5
    elseif t >= T4
        return 4
    elseif t >= TB3
        return 3
    else
        return 0
    end
end

# Compute a signature value between 0 and 19682 (== 3^(4+1+4)-1) for the line of seven stones
#
# The current line is centered at (y,x) at position [0].
# [-4] [-3] [-2] [-1] [0] [ 1] [ 2] [ 3] [ 4]
function line_to_value(line)
    @assert length(line) == (4+4+1)
    res = 0
    for v in line
        res *= 3
        @assert v >= -1 && v <= 1
        v += 1
        res += v
    end
    res
end
max_line_value = 19682
function value_to_line(value::Int)
    @assert value >= 0 && value <= max_line_value
    line = zeros(Int8, 4+1+4)
    for i = length(line):-1:1
        value, rem = divrem(value, 3)
        line[i] = rem - 1
    end
    line
end


