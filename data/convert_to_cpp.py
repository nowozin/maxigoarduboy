
import json

data = json.load(open("threats-lookup-table.json"));

def write_array(out, arrayname):
    array = data[arrayname]
    n = len(array)
    print("Writing %d items of array '%s'" % (n, arrayname))
    print("uint8_t %s[] = {" % arrayname, file=out)
    value = 0;
    for (i, v) in enumerate(array):
        v = int(v)
        if i & 1 == 1:
            v <<= 4 # shift 4 bits to occupy high byte half

        value += v
        if i & 1 == 1:  # odd, one byte complete, write out
            print("    0x%02x," % value, file=out)
            value = 0   # reset
    if i & 1 == 0:  # incomplete half-byte remaining, write out
        print("    0x%02x," % value, file=out)
    print("};", file=out)

with open("threatstable.h", "w") as out:
    print("#ifndef THREATSTABLE_H", file=out)
    print("#define THREATSTABLE_H", file=out)
    write_array(out, "black_threats")
    write_array(out, "white_threats")
    print("#endif", file=out)

