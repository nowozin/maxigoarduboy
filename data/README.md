
# Information

This directory contains code and data to generate the `threatstable.h` and
`threatstable24.h` file.

The data in the JSON file comes from the Gomoku engine I wrote at
`http://maxigo.eu/` and can be re-created using the included notebook.

Both files are already included in the codebase, so the code/data in this
directory is only relevant for developers interested in modififying the
internals of the Gomoku engine itself.


# Generating the `threatstable.h` file

Steps:

1. Run the Jupyter Notebook using [Julia 1.6](https://julialang.org/) or
   higher.
2. Run the `convert_to_cpp.py` file.


# Generating the `threatstable24.h` file

This file was used in experimental ideas to compress the lookup table further.
In the end this code is not used because while it saved ROM space the negative
impact on speed was too large.

Steps:

1. Run the Jupyter Notebook using [Julia 1.6](https://julialang.org/) or
   higher.  This directly generates the `threatstable24.h` file.


