#ifdef PERFORM_UNIT_TESTS

#include <Arduboy2.h>
#include <AUnitVerbose.h>

#define BOARD_SIZE  15
#include "board.h"
Board board;

const piece_t signMapThreats[BOARD_SIZE][BOARD_SIZE] = {
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, -1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, -1, 0, 1, 1, 1, 0, 0, 0},
  {0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1}
};

#ifdef TEST_LOOKUP
test(BoardTest, Orders) {
  assertEqual(5, order(TWin));
  assertEqual(4, order(TS4));
  assertEqual(4, order(T4));
  assertEqual(3, order(TS3));
  assertEqual(3, order(T3));
  assertEqual(3, order(TB3));
  assertEqual(0, order(TN));
}

test(BoardTest, ThreatsLookup) {
  // Black
  assertEqual(TN, threats_lookup(8321, STONE_BLACK));
  assertEqual(TN, threats_lookup(9345, STONE_BLACK));
  assertEqual(TN, threats_lookup(9763, STONE_BLACK));
  assertEqual(TN, threats_lookup(13387, STONE_BLACK));
  assertEqual(TN, threats_lookup(14140, STONE_BLACK));
  assertEqual(TN, threats_lookup(14584, STONE_BLACK));
  assertEqual(TN, threats_lookup(14832, STONE_BLACK));
  assertEqual(TN, threats_lookup(17755, STONE_BLACK));
  assertEqual(TN, threats_lookup(17776, STONE_BLACK));

  assertEqual(TS4, threats_lookup(1213, STONE_BLACK));
  assertEqual(TS3, threats_lookup(3630, STONE_BLACK));
  assertEqual(TWin, threats_lookup(4370, STONE_BLACK));
  assertEqual(T4, threats_lookup(9692, STONE_BLACK));
  assertEqual(T4, threats_lookup(12859, STONE_BLACK));
  assertEqual(T4, threats_lookup(14821, STONE_BLACK));
  assertEqual(TB3, threats_lookup(16495, STONE_BLACK));
  assertEqual(T3, threats_lookup(16518, STONE_BLACK));
  assertEqual(TWin, threats_lookup(19621, STONE_BLACK));
}

test(BoardTest, LookupValues) {
  displaySerial(signMapThreats);

  Board ai(signMapThreats, STONE_BLACK);

  piece_t threat_player = STONE_BLACK;
  piece_t outside = STONE_WHITE;

  assertEqual(signMapThreats[14][0], STONE_BLACK);
  assertEqual(signMapThreats[14][1], STONE_BLACK);
  assertEqual(signMapThreats[14][2], STONE_BLACK);
  assertEqual(signMapThreats[14][3], STONE_BLACK);
  assertEqual(signMapThreats[14][4], STONE_BLACK);

  assertEqual(((lookup_t) 242),
    ai.compute_lookup_value(14, 0, 0, 1, outside, threat_player == STONE_WHITE));
  assertEqual(((lookup_t) 727),
    ai.compute_lookup_value(14, 1, 0, 1, outside, threat_player == STONE_WHITE));
  assertEqual(((lookup_t) 2182),
    ai.compute_lookup_value(14, 2, 0, 1, outside, threat_player == STONE_WHITE));
  assertEqual(((lookup_t) 6547),
    ai.compute_lookup_value(14, 3, 0, 1, outside, threat_player == STONE_WHITE));
  assertEqual(((lookup_t) 19642),
    ai.compute_lookup_value(14, 4, 0, 1, outside, threat_player == STONE_WHITE));
}

test(BoardTest, PossibleThreats) {
  Board ai(signMapThreats, STONE_BLACK);
  piece_t threat_player = STONE_BLACK;
  piece_t outside = STONE_WHITE;

  // Three to Four
  ai.display();
  assertEqual(T4, ai.lookup_possible_threat(7, 8, outside, threat_player));

  // Three to Straight Four
  assertEqual(TS4, ai.lookup_possible_threat(7, 12, outside, threat_player));

  // Broken Three to Straight Four
  assertEqual(TS4, ai.lookup_possible_threat(9, 5, outside, threat_player));

  // Straight Four to Win
  assertEqual(TWin, ai.lookup_possible_threat(7, 1, outside, threat_player));
}
#endif

#ifdef TEST_THREATS
test(BoardTest, ThreatScoring) {
  assertMore(threat_to_score(TWin), threat_to_score(T4));
  assertMore(threat_to_score(T4), threat_to_score(T3));
  assertMore(threat_to_score(TB3), threat_to_score(TN));
}

test(BoardTest, Threats) {
  Board ai(signMapThreats, STONE_BLACK);

  piece_t threat_player = STONE_BLACK;
  piece_t outside = STONE_WHITE;

  assertEqual(TWin, ai.lookup_threat(14, 0, 0, 1, outside, threat_player));
  assertEqual(TWin, ai.lookup_threat(14, 1, 0, 1, outside, threat_player));
  assertEqual(TWin, ai.lookup_threat(14, 2, 0, 1, outside, threat_player));
  assertEqual(TWin, ai.lookup_threat(14, 3, 0, 1, outside, threat_player));
  assertEqual(TWin, ai.lookup_threat(14, 4, 0, 1, outside, threat_player));

  // Straight four
  assertEqual(TS4, ai.lookup_threat(8, 1, 1, 0, outside, threat_player));
  assertEqual(TS4, ai.lookup_threat(9, 1, 1, 0, outside, threat_player));
  assertEqual(TS4, ai.lookup_threat(10, 1, 1, 0, outside, threat_player));
  assertEqual(TS4, ai.lookup_threat(11, 1, 1, 0, outside, threat_player));

  // Four: oxxxx.
  assertEqual(T4, ai.lookup_threat(1, 2, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(1, 3, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(1, 4, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(1, 5, 0, 1, outside, threat_player));

  // Four: x.xxx
  assertEqual(T4, ai.lookup_threat(14, 10, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(14, 12, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(14, 13, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(14, 14, 0, 1, outside, threat_player));

  // Four: xx.xx
  assertEqual(T4, ai.lookup_threat(12, 10, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(12, 11, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(12, 13, 0, 1, outside, threat_player));
  assertEqual(T4, ai.lookup_threat(12, 14, 0, 1, outside, threat_player));
  
  // Straight three
  assertEqual(TS3, ai.lookup_threat(4, 9, 0, 1, outside, threat_player));
  assertEqual(TS3, ai.lookup_threat(4, 10, 0, 1, outside, threat_player));
  assertEqual(TS3, ai.lookup_threat(4, 11, 0, 1, outside, threat_player));

  // Broken three 
  assertEqual(TB3, ai.lookup_threat(8, 4, 1, 1, outside, threat_player));
  assertEqual(TB3, ai.lookup_threat(10, 6, 1, 1, outside, threat_player));
  assertEqual(TB3, ai.lookup_threat(11, 7, 1, 1, outside, threat_player));

  // Three
  assertEqual(T3, ai.lookup_threat(7, 9, 0, 1, outside, threat_player));
  assertEqual(T3, ai.lookup_threat(7, 10, 0, 1, outside, threat_player));
  assertEqual(T3, ai.lookup_threat(7, 11, 0, 1, outside, threat_player));
}
#endif

const piece_t signMapHeuristic[BOARD_SIZE][BOARD_SIZE] = {
  { 0, 0, -1, 0, 0, 0, 1, 0, 0, 0, 0, 0, -1, 1, 0},
  { 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0, 0},
  { 0, 1, 1, 0, 0, 0, -1, 0, 0, 0, -1, 0, -1, 0, 1},
  { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0},
  { 1, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
  { 1, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
  { 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0},
  { 0, 1, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  { -1, 0, 1, 0, 0, 0, -1, 1, 0, -1, 0, -1, -1, 0, 1},
  { 1, 1, 0, 0, -1, -1, 0, -1, -1, 0, -1, 0, 0, 0, 0},
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0},
  { 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0},
  { 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 1, -1, 0, 0, -1},
  { 1, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1}
};

#ifdef TEST_HEURISTIC
test(HeuristicTest, HeuristicScoring) {
  Board ai(signMapHeuristic, STONE_BLACK);

  assertEqual(-1018500, ai.heuristic_evaluation());
}

test(HeuristicTest, HighestThreat) {
  Board ai(signMapHeuristic, STONE_BLACK);

  assertEqual(TS3, ai.highest_existing_threat(STONE_BLACK));
  assertEqual(T4, ai.highest_existing_threat(STONE_WHITE));
}

test(HeuristicTest, NearbyLogic) {
  Board ai(signMapHeuristic, STONE_BLACK);

  assertEqual(false, ai.has_nearby(12, 1, 1));
  assertEqual(true, ai.has_nearby(12, 1, 2));
}
#endif

#ifdef TEST_AI
test(AITest, NegaMax) {
  Board ai(signMapHeuristic, STONE_BLACK);

  assertEqual(true, Move(-6500, 11-1, 7-1) == ai.negamax(1, STONE_BLACK));
  assertEqual(true, Move(-2006200, 11-1, 7-1) == ai.negamax(2, STONE_BLACK));
  //assertEqual(true, Move(-1006000, 11-1, 7-1) == ai.negamax(3, STONE_BLACK));
  //assertEqual(true, Move(-100000000, 11-1, 7-1) == ai.negamax(4, STONE_BLACK));

  assertEqual(true, Move(100000000, 11-1, 7-1) == ai.negamax(1, STONE_WHITE));
  assertEqual(true, Move(100000000, 11-1, 7-1) == ai.negamax(2, STONE_WHITE));
  //assertEqual(true, Move(100000000, 2-1, 8-1) == ai.negamax(3, STONE_WHITE));
  //assertEqual(true, Move(100000000, 2-1, 8-1) == ai.negamax(4, STONE_WHITE));
}
#endif

void setup() {
//  arduboy.begin();
//  arduboy.setFrameRate(10);
//  arduboy.initRandomSeed();

  delay(1000); // wait for stability on some boards to prevent garbage Serial
  Serial.begin(115200); // ESP8266 default of 74880 not supported on Linux
  while(!Serial); // for the Arduino Leonardo/Micro only

  Serial.println(F("Running maxigoTest"));
  Serial.println(F("----"));

  board.reset();
}

void loop() {
  aunit::TestRunner::setTimeout(600);
  aunit::TestRunner::run();
}

#endif
