// Enable to define below to perform unit testing.
//#define PERFORM_UNIT_TESTS

// Then choose a set of tests to enable (not all may fit into program memory)
//#define TEST_LOOKUP
//#define TEST_THREATS
#define TEST_AI
//#define TEST_HEURISTIC

#ifndef PERFORM_UNIT_TESTS

// Optimization: edit /usr/share/arduino/hardware/arduino/avr/platform.txt

#include <assert.h>
#include <Arduboy2.h>

#include "assets.h"

#define BOARD_SIZE  13
//#define BOARD_SIZE  15
#include "board.h"
Board board;

uint8_t cursor_color = WHITE;
#define CURSOR_DEFAULT_X  6
#define CURSOR_DEFAULT_Y  6
int cursor_x = CURSOR_DEFAULT_X;
int cursor_y = CURSOR_DEFAULT_Y;

#include "movestack.h"
MoveStack moves;

Arduboy2 arduboy;

#define BOARD_STONE_PIXEL_OFFSET  5
#define BOARD_TOP_LEFT_X  32
#define BOARD_TOP_LEFT_Y  0
#define BOARD_TOP_RIGHT_X (BOARD_TOP_LEFT_X+BOARD_SIZE*BOARD_STONE_PIXEL_OFFSET)
#define BOARD_TOP_RIGHT_Y BOARD_TOP_LEFT_Y
#define BOARD_BOTTOM_LEFT_X BOARD_TOP_LEFT_X
#define BOARD_BOTTOM_LEFT_Y (BOARD_TOP_LEFT_Y+BOARD_SIZE*BOARD_STONE_PIXEL_OFFSET)
#define BOARD_BOTTOM_RIGHT_X  BOARD_TOP_RIGHT_X
#define BOARD_BOTTOM_RIGHT_Y  BOARD_BOTTOM_LEFT_Y

#define BOARD15_STONE_PIXEL_OFFSET  4
#define BOARD15_TOP_LEFT_X  32
#define BOARD15_TOP_LEFT_Y  2
#define BOARD15_TOP_RIGHT_X (BOARD15_TOP_LEFT_X+BOARD_SIZE*BOARD15_STONE_PIXEL_OFFSET)
#define BOARD15_TOP_RIGHT_Y BOARD15_TOP_LEFT_Y
#define BOARD15_BOTTOM_LEFT_X BOARD15_TOP_LEFT_X
#define BOARD15_BOTTOM_LEFT_Y (BOARD15_TOP_LEFT_Y+BOARD_SIZE*BOARD15_STONE_PIXEL_OFFSET)
#define BOARD15_BOTTOM_RIGHT_X  BOARD15_TOP_RIGHT_X
#define BOARD15_BOTTOM_RIGHT_Y  BOARD15_BOTTOM_LEFT_Y


#define WIN_X BOARD_TOP_LEFT_X
#define WIN_Y_TOP (BOARD_TOP_LEFT_Y+2)
#define WIN_Y_BOTTOM (BOARD_BOTTOM_LEFT_Y-12)

enum GameState {
  START_SCREEN = 0,
  TWOPLAYER_P1_MOVE = 1,
  TWOPLAYER_P2_MOVE = 2,
  TWOPLAYER_P1_WON = 3,
  TWOPLAYER_P2_WON = 4,
  TWOPLAYER_DRAW = 5,
  ONEPLAYER_P_MOVE = 6,
  ONEPLAYER_AI_MOVE = 7,
  ONEPLAYER_P_WON = 8,
  ONEPLAYER_AI_WON = 9,
  ONEPLAYER_DRAW = 10,
};
GameState gamestate = START_SCREEN;


// Undo functions
bool in_undo_mode = false;
uint8_t ptr_undo;
uint8_t ptr_redo = 0;

void updateUndo(bool singleplayer) {
  if (arduboy.justPressed(B_BUTTON)) {
    // Just entered UNDO mode
    ptr_undo = moves.ptr;
    in_undo_mode = true;
  } if (arduboy.justReleased(B_BUTTON)) {
    // Apply undo sequence
    in_undo_mode = false; // disable undo mode
    if (ptr_undo == moves.ptr)  // nothing to do
      return;

    // Perform undo
    //   1. erase all moves on board from replay point onwards
    for (uint8_t ptr = ptr_undo; ptr < moves.ptr; ++ptr) {
      Move move = from_moveb(moves.stack[ptr]);
      board.remove(move.y, move.x);
    }
    // Handle redo properly
    if (ptr_undo > moves.ptr) {
      // Redo: need to _make_ moves.
      for (uint8_t ptr = moves.ptr; ptr < ptr_undo; ++ptr) {
        Move move = from_moveb(moves.stack[ptr]);
        piece_t player = player_to_play(ptr);
        board.move(move.y, move.x, player);
      }
      if (ptr_undo == ptr_redo) {
        ptr_redo = 0;  // no further redo possible
      } else {
        // We have (ptr_undo < ptr_redo) and we still can
        // "walk forwards" to replay the game, which is useful.
      }
    } else {
      // Simple undo
      ptr_redo = moves.ptr;
    }
    moves.ptr = ptr_undo;
    piece_t player_now = player_to_play(ptr_undo);

    // Re-find cursor and determine if the game has ended
    uint8_t n = moves.len();
    if (n > 0) {
      Move move = from_moveb(moves.stack[n-1]);
      cursor_y = move.y;
      cursor_x = move.x;
    } else {
      cursor_y = CURSOR_DEFAULT_Y;
      cursor_x = CURSOR_DEFAULT_X;
    }
    bool has_game_ended = board.winner_at(cursor_y, cursor_x);

    if (singleplayer) {
      // One player
      if (has_game_ended) {
        gamestate = (player_now == STONE_BLACK) ?
          ONEPLAYER_AI_WON : ONEPLAYER_P_WON;
      } else {
        gamestate = ONEPLAYER_P_MOVE;
      }
    } else {
      // Two player
      if (has_game_ended) {
        // We are "one stone ahead" with player_now
        gamestate = (player_now == STONE_BLACK) ?
          TWOPLAYER_P2_WON : TWOPLAYER_P1_WON;        
      } else {
        gamestate = (player_now == STONE_BLACK) ?
          TWOPLAYER_P1_MOVE : TWOPLAYER_P2_MOVE;
      }
    }
    return;
  }

  if (in_undo_mode == false)
    return;

  // Undo/redo left/right actions
  if (arduboy.justReleased(LEFT_BUTTON)) {
    if (singleplayer && ptr_undo >= 2) {
      if (ptr_undo % 2 == 0) {
        ptr_undo -= 2;
      } else {
        ptr_undo -= 1;
      }
    } else if (singleplayer == false && ptr_undo > 0) {
      ptr_undo -= 1;
    }
  } else if (arduboy.justReleased(RIGHT_BUTTON)) {
    uint8_t ptr_max = moves.ptr;
    if (ptr_redo > 0)
      ptr_max = ptr_redo; // Still not made a move after undo

    if (singleplayer
      && ptr_undo % 2 == 0  // - only allow to skip between player moves
      && moves.ptr >= 2     // - there must be sufficient number of moves
      && ptr_undo < ptr_max)  // - not at the end yet
    {
      ptr_undo += 2;
      if (ptr_undo > ptr_max)
        ptr_undo = ptr_max;
    } else if (singleplayer == false
      && ptr_undo < ptr_max)
    {
      ptr_undo += 1;
    }
  }
}


// Drawing functions
void drawStone13(pos_t bx, pos_t by, piece_t color) {
  if (color == STONE_EMPTY) // nothing to draw
    return;

  // Board to pixel position
  int x = BOARD_TOP_LEFT_X + bx*BOARD_STONE_PIXEL_OFFSET;
  int y = BOARD_TOP_LEFT_Y + by*BOARD_STONE_PIXEL_OFFSET;

  const unsigned char* stone = (color == STONE_BLACK) ?
    black44_plus_mask : white44_plus_mask;
  Sprites::drawPlusMask(x, y, stone, 0);   // frame=0
}

void drawMarker13(int bx, int by) {
#if 0
  // Only draw marker if corner is currently empty
  if (board.get(by, bx) != STONE_EMPTY)
    return;
#endif

  int x = BOARD_TOP_LEFT_X + bx*BOARD_STONE_PIXEL_OFFSET;
  int y = BOARD_TOP_LEFT_Y + by*BOARD_STONE_PIXEL_OFFSET;

  arduboy.drawRect(x+1, y+1, 2, 2, WHITE);
}

void drawBoard13(void) {
  // Draw corners of the board
  drawMarker13(0, 0);
  drawMarker13(0, BOARD_SIZE-1);
  drawMarker13(BOARD_SIZE-1, 0);
  drawMarker13(BOARD_SIZE-1, BOARD_SIZE-1);

#if 0
  for (int by = 0; by < BOARD_SIZE; ++by)
    for (int bx = 0; bx < BOARD_SIZE; ++bx)
      drawStone13(bx, by, board.get(by, bx));
#endif

#ifdef DEBUG_CONSISTENCY
  // Check consistency of movestack with board
  int inconsistent = 0;
  for (uint8_t ptr = 0; ptr < moves.ptr; ++ptr) {
    Move move = from_moveb(moves.stack[ptr]);
    piece_t player = player_to_play(ptr);
    if (board.get(move.y, move.x) != player)
      inconsistent += 1;
  }
  arduboy.setCursor(0, 50);
  arduboy.print(inconsistent);
#endif

#if 1
  // Draw all stones as player until end or replay point
  uint8_t ptr_max = moves.ptr;
  if (in_undo_mode)
    ptr_max = ptr_undo; // display only until current replay point

  for (uint8_t ptr = 0; ptr < ptr_max; ++ptr) {
    Move move = from_moveb(moves.stack[ptr]);
    piece_t player = player_to_play(ptr);
    drawStone13(move.x, move.y, player);
  }
#endif

  arduboy.setCursor(0, 0);
  if (in_undo_mode) {
    arduboy.print(ptr_undo);
    arduboy.setCursor(0, 20);
    arduboy.print("<-->");
    arduboy.setCursor(0, 30);
    arduboy.print("UNDO");
  } else {
    // Stack depth
    arduboy.print(moves.len());
  }
}

void drawStone15(int bx, int by, int color) {
  if (color == STONE_EMPTY)
    return;

  // Board to pixel position
  int x = BOARD15_TOP_LEFT_X + bx*BOARD15_STONE_PIXEL_OFFSET;
  int y = BOARD15_TOP_LEFT_Y + by*BOARD15_STONE_PIXEL_OFFSET;

  const unsigned char* stone = (color == STONE_BLACK) ? black33 : white33;
  Sprites::drawOverwrite(x, y, stone, 0);   // frame=0
}

void drawBoard15(void) {
    for (int by = 0; by < BOARD_SIZE; ++by)
      for (int bx = 0; bx < BOARD_SIZE; ++bx)
        drawStone15(bx, by, board.get(by, bx));

    // Top border
    arduboy.drawLine(BOARD15_TOP_LEFT_X-2, BOARD15_TOP_LEFT_Y-2,
      BOARD15_TOP_RIGHT_X, BOARD15_TOP_RIGHT_Y-2, WHITE);
    // Bottom border
    arduboy.drawLine(BOARD15_BOTTOM_LEFT_X-1, BOARD15_BOTTOM_LEFT_Y,
      BOARD15_BOTTOM_RIGHT_X-1, BOARD15_BOTTOM_RIGHT_Y, WHITE);
    // Left border
    arduboy.drawLine(BOARD15_TOP_LEFT_X-2, BOARD15_TOP_LEFT_Y-2,
      BOARD15_BOTTOM_LEFT_X-2, BOARD15_BOTTOM_LEFT_Y, WHITE);
    // Right border
    arduboy.drawLine(BOARD15_TOP_RIGHT_X, BOARD15_TOP_RIGHT_Y-1,
      BOARD15_BOTTOM_RIGHT_X, BOARD15_BOTTOM_RIGHT_Y, WHITE);
}

void drawStoneRect13(int bx, int by, int color = WHITE) {
  int x = BOARD_TOP_LEFT_X + bx*BOARD_STONE_PIXEL_OFFSET;
  int y = BOARD_TOP_LEFT_Y + by*BOARD_STONE_PIXEL_OFFSET;
  arduboy.drawRect(x-1, y-1,
    BOARD_STONE_PIXEL_OFFSET+1, BOARD_STONE_PIXEL_OFFSET+1, color);
}

void drawStoneRect15(int bx, int by, int color = WHITE) {
  int x = BOARD15_TOP_LEFT_X + bx*BOARD15_STONE_PIXEL_OFFSET;
  int y = BOARD15_TOP_LEFT_Y + by*BOARD15_STONE_PIXEL_OFFSET;
  arduboy.drawRect(x-1, y-1,
    BOARD15_STONE_PIXEL_OFFSET+1, BOARD15_STONE_PIXEL_OFFSET+1,
    color);
}

void drawStoneRect(int bx, int by, int color = WHITE) {
  drawStoneRect13(bx, by, color);
}

void drawBoard(void) {
  drawBoard13();
}

void drawCursor(void) {
  drawStoneRect13(cursor_x, cursor_y, cursor_color);
}



enum AIPlayingLevel {
  BABY_AI = 0,
  NEGAMAX_1 = 1,
};
AIPlayingLevel ai_level = BABY_AI; //NEGAMAX_1;

enum StartCursorState {
  PvAIeasy = 0,
  PvAIhard = 1,
  PvP = 2,
};
int start_select = PvAIeasy;

void resetBoard(void) {
  board.reset();
  moves.reset();
  ptr_redo = 0;

  start_select = PvAIeasy;

  cursor_x = CURSOR_DEFAULT_X;
  cursor_y = CURSOR_DEFAULT_Y;
}

void setup() {
  arduboy.begin();
  arduboy.setFrameRate(10);
  arduboy.initRandomSeed();

  resetBoard();
  // For visual testing: initialize board randomly
  //board.setRandomState();
}

#define SS_OPT_X 57
#define SS_OPT_X_CARRET 8
#define SS_OPT_Y 20
#define SS_OPT_YLINE  10

bool moveButtonPressed(void) {
  return arduboy.justReleased(A_BUTTON); // || arduboy.justReleased(B_BUTTON);
}

// Handle game selection/start screen
void handleStartScreen(void) {
  Sprites::drawOverwrite(0, 1, maxigo_arduboy, 0);   // frame=0

  arduboy.setCursor(SS_OPT_X, SS_OPT_Y);
  arduboy.print("P vs AI easy");
  arduboy.setCursor(SS_OPT_X, SS_OPT_Y+SS_OPT_YLINE);
  arduboy.print("P vs AI hard");
  arduboy.setCursor(SS_OPT_X, SS_OPT_Y+2*SS_OPT_YLINE);
  arduboy.print("P1 vs P2");

  int carret_y = 0;
  switch (start_select) {
    case PvAIeasy:
      carret_y = SS_OPT_Y;
      break;
    case PvAIhard:
      carret_y = SS_OPT_Y + SS_OPT_YLINE;
      break;
    case PvP:
      carret_y = SS_OPT_Y + 2*SS_OPT_YLINE;
      break;
  }
  arduboy.setCursor(SS_OPT_X-SS_OPT_X_CARRET, carret_y);
  arduboy.print(">");

  if (arduboy.justPressed(UP_BUTTON)) {
    start_select -= 1;
    if (start_select < 0)
      start_select = PvP;
  }
  if (arduboy.justPressed(DOWN_BUTTON)) {
    start_select += 1;
    if (start_select > PvP)
      start_select = PvAIeasy;
  }

  if (moveButtonPressed()) {
    switch (start_select) {
      case PvAIeasy:
        gamestate = ONEPLAYER_P_MOVE;
        ai_level = BABY_AI;
        break;
      case PvAIhard:
        gamestate = ONEPLAYER_P_MOVE;
        ai_level = NEGAMAX_1;
        break;
      case PvP:
        gamestate = TWOPLAYER_P1_MOVE;
        break;
    }
  }
}

int setWinnerCursor(void) {
  int y = (cursor_y >= (BOARD_SIZE >> 1)) ? WIN_Y_TOP : WIN_Y_BOTTOM;
  arduboy.setCursor(WIN_X, y);
  return y;
}

void printWinnerMessage(const char* message) {
  int win_y = setWinnerCursor();
  arduboy.fillRect(20, win_y - 2, 98, 12, BLACK);
  arduboy.drawRect(20, win_y - 2, 98, 12, WHITE);

  arduboy.print(message);
}

void drawWinningLines(void) {
  for (pos_t y = 0; y < BOARD_SIZE; ++y) {
    for (pos_t x = 0; x < BOARD_SIZE; ++x) {
      threat_t tblack = board.highest_existing_threat_at(y, x, STONE_BLACK);
      threat_t twhite = board.highest_existing_threat_at(y, x, STONE_WHITE);
      if (tblack == TWin || twhite == TWin)
        drawStoneRect(x, y, WHITE);
    }
  }
}

void handleTwoPlayerResult(void) {
  drawBoard();
  updateUndo(false);
  if (in_undo_mode || gamestate == TWOPLAYER_P1_MOVE || gamestate == TWOPLAYER_P2_MOVE)
    return;

  drawWinningLines();

  if (gamestate == TWOPLAYER_P1_WON) {
    printWinnerMessage("Player 1 won!");
  } else if (gamestate == TWOPLAYER_P2_WON) {
    printWinnerMessage("Player 2 won!");
  } else {
    assert(gamestate == TWOPLAYER_DRAW);
    printWinnerMessage("It is a draw!");
  }

  if (moveButtonPressed()) {
    gamestate = START_SCREEN;
    resetBoard();
  }
}

void handleOnePlayerResult(void) {
  drawBoard();
  updateUndo(true);
  if (in_undo_mode || gamestate == ONEPLAYER_P_MOVE)
    return;

  drawWinningLines();

  if (gamestate == ONEPLAYER_P_WON) {
    printWinnerMessage("The human won!");
  } else if (gamestate == ONEPLAYER_AI_WON) {
    printWinnerMessage("The AI won!");
  } else {
    assert(gamestate == ONEPLAYER_DRAW);
    printWinnerMessage("It is a draw!");
  }

  if (moveButtonPressed()) {
    gamestate = START_SCREEN;
    resetBoard();
  }
}

void updateCursor(void) {
  // Make cursor flicker
  cursor_color = (cursor_color == WHITE) ? BLACK : WHITE;

  // Undo mode (B button): do not update cursor
  if (arduboy.pressed(B_BUTTON))
    return;

  // Update cursor
  if (arduboy.justPressed(UP_BUTTON) && cursor_y > 0)
    cursor_y -= 1;
  if (arduboy.justPressed(DOWN_BUTTON) && cursor_y < (BOARD_SIZE-1))
    cursor_y += 1;
  if (arduboy.justPressed(LEFT_BUTTON) && cursor_x > 0)
    cursor_x -= 1;
  if (arduboy.justPressed(RIGHT_BUTTON) && cursor_x < (BOARD_SIZE-1))
    cursor_x += 1;
}

void makeMove(pos_t y, pos_t x, piece_t color) {
  board.move(y, x, color);
  moves.push(Move(0, y, x));
  ptr_redo = 0;  // invalidate any redo actions
}

void handleOnePlayerGame(void) {
  // Player move
  if (gamestate == ONEPLAYER_P_MOVE) {
    updateUndo(true);

    if (in_undo_mode == false
      && board.get(cursor_y, cursor_x) == STONE_EMPTY
      && moveButtonPressed())
    {
      makeMove(cursor_y, cursor_x, STONE_BLACK);

      if (board.winner_at(cursor_y, cursor_x)) {
        gamestate = ONEPLAYER_P_WON;
      } else {
        gamestate = ONEPLAYER_AI_MOVE;
        if (board.is_full())
          gamestate = ONEPLAYER_DRAW;
      }
    }
    drawBoard();
    updateCursor();
    if (gamestate == ONEPLAYER_P_MOVE) {
      drawCursor();
    }

    return;
  }

  // AI Move
#define NEGAMAX_LEVEL 1
  assert(gamestate == ONEPLAYER_AI_MOVE);
  Move move(0, 0, 0);
  if (ai_level == BABY_AI) {
    // Stochastically mix:
    //    20 percent: baby AI
    //    80 percent: negamax AI
    if (random(0, 101) <= 20) {
      move = board.baby_ai();
    } else {
      move = board.negamax(NEGAMAX_LEVEL, STONE_WHITE);
    }
  } else if (ai_level == NEGAMAX_1) {
    move = board.negamax(NEGAMAX_LEVEL, STONE_WHITE);
  }
  makeMove(move.y, move.x, STONE_WHITE);

  // Let player know where the move happened by moving the cursor
  cursor_y = move.y;
  cursor_x = move.x;
  if (board.winner_at(move.y, move.x)) {
    gamestate = ONEPLAYER_AI_WON;
    //displaySerial(board.signMap);
  } else {
    gamestate = ONEPLAYER_P_MOVE;
  }
}

void handleTwoPlayerGame(void) {
  drawBoard();

  updateCursor();
  updateUndo(false);
  drawCursor();

  if (in_undo_mode == false
    && board.get(cursor_y, cursor_x) == STONE_EMPTY
    && moveButtonPressed())
  {
    if (gamestate == TWOPLAYER_P1_MOVE) {
      makeMove(cursor_y, cursor_x, STONE_BLACK);
    } else {
      assert(gamestate == TWOPLAYER_P2_MOVE);
      makeMove(cursor_y, cursor_x, STONE_WHITE);
    }

    // Determine if this was a winning move
    if (board.winner_at(cursor_y, cursor_x)) {
      // Win:
      gamestate = (gamestate == TWOPLAYER_P1_MOVE) ?
        TWOPLAYER_P1_WON : TWOPLAYER_P2_WON;
    } else {
      // No win: the other player's turn
      gamestate = (gamestate == TWOPLAYER_P1_MOVE) ?
        TWOPLAYER_P2_MOVE : TWOPLAYER_P1_MOVE;

      if (board.is_full())
        gamestate = TWOPLAYER_DRAW;
    }
  }
}

void loop() {
  if (!(arduboy.nextFrame()))
    return;

  arduboy.pollButtons();

  // Draw board
  arduboy.clear();

  switch (gamestate) {
    case START_SCREEN:
      handleStartScreen();
      break;
    case TWOPLAYER_P1_MOVE:
    case TWOPLAYER_P2_MOVE:
      handleTwoPlayerGame();
      break;
    case TWOPLAYER_P1_WON:
    case TWOPLAYER_P2_WON:
    case TWOPLAYER_DRAW:
      handleTwoPlayerResult();
      break;
    case ONEPLAYER_P_MOVE:
    case ONEPLAYER_AI_MOVE:
      handleOnePlayerGame();
      break;
    case ONEPLAYER_P_WON:
    case ONEPLAYER_AI_WON:
    case ONEPLAYER_DRAW:
      handleOnePlayerResult();
      break;
  }

  arduboy.display();
}

#endif
