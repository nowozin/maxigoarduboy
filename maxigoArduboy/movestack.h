#ifndef MOVESTACK_H
#define MOVESTACK_H

#include <assert.h>
#include "board.h"

// We encode moves as bytes via yyyy.xxxx, 4-bit unsigned.
#define MOVE_EMPTY  0x00
typedef uint8_t moveb_t;

moveb_t to_moveb(const Move& move) {
  assert(move.y >= 0 && move.y < BOARD_SIZE);
  assert(move.x >= 0 && move.x < BOARD_SIZE);

  uint8_t res = ((move.y & 0x0f) << 4) | (move.x & 0x0f);
  return res;
}

Move from_moveb(moveb_t moveb) {
  pos_t y = (moveb >> 4) & 0x0f;
  pos_t x = moveb & 0x0f;

  return Move(0, y, x);
}

piece_t player_to_play(uint8_t ptr) {
  return (ptr % 2 == 0) ? STONE_BLACK : STONE_WHITE;
}

#define STACK_SIZE  (BOARD_SIZE*BOARD_SIZE)
//#define STACK_SIZE  20

class MoveStack {
public:
  uint8_t ptr;  // Points to a free space just above top of stack
  moveb_t stack[STACK_SIZE];

  void reset(void) {
    ptr = 0;
    for (uint8_t p = 0; p < STACK_SIZE; ++p)
      stack[p] = MOVE_EMPTY;
  }

  MoveStack(void) {
    reset();
  }

  bool is_full(void) const {
    return ptr == STACK_SIZE;
  }
  void push(const Move& move) {
    assert(!is_full());
    stack[ptr] = to_moveb(move);
    ptr += 1;
  }
  Move pop(void) {
    assert(ptr > 0);  // element to pop is available;
    ptr -= 1;
    return from_moveb(stack[ptr]);
  }
  uint8_t len(void) const {
    return ptr;
  }
};

#endif
